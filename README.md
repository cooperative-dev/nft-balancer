### NFT Sell-only Balancer

Balancer is a bit of a misnomer here. This program is meant to be used in a model where some percent of the royalties from the NFT sales are sent to the treasury in this program, and it offers a measure of instant liquidity for those wishing to sell their NFT. The price does not change as a result of NFTs being sold into the balancer.

#### Basic architecture
1. In the first step we initialize a 'treasury' account (to hold lamports), and a 'treasury_data' pda (to hold data).
2. The program deployer is then responsible for submitting (one by one) each mint address for the collection.
3. When the mint addresses are submitted, a 'mint_check' pda is created, which basically says: "yes this mint is valid, and you can check it in the future when people try to sell their NFTs". So there will be one mint_check per NFT in the collection.
4. After the mints have all been submitted, the deployer must call the toggle function 'freeze_adding_mints' to finish deployment and enable NFT sales.
5. When someone sells, a new token account is created with the treasury_data pda as the owner. The token is then transferred, and the old token account closed.
6. When an admin comes to withdraw the sold NFTs it is the same create/transfer process.

#### Improvements
* The frontend could be greatly improved - I'm not a frontend developer by trade. For starters: it's not DRY and the CSS is garbage. Any pull requests would be welcomed.
* If you find a bug in the Anchor/Solana program and DM us, we will give you a free NFT/membership to [@cooperative_dev](https://twitter.com/cooperative_dev)

#### Thanks
* [@armaniferrante](https://twitter.com/armaniferrante) for his work on Anchor
* [@cqfdee](https://twitter.com/cqfdee) for his support in questions on Discord
* [@PierreArowana](https://twitter.com/PierreArowana) for opensourcing [@RAM_NFT](https://twitter.com/RAM_NFT)
* [@0xGoki](https://twitter.com/0xGoki) for creating [WalletKit](https://github.com/GokiProtocol/walletkit)

#### You too can have an NFT Balancer for your collection/DAO! :tada:
I'm happy to help set this up for anyone who wants to use this. There is a 0.5% developer royalty fee which goes to [@cooperative_dev](https://twitter.com/cooperative_dev).
Please keep in mind that even if we help to set this up for your project, this code has not been audited and comes with no guarantees.

Author: [@joebuild](https://twitter.com/joebuild)

![screenshot](balancer_screenshot.png?raw=true)