import {createTheme, ThemeProvider} from '@material-ui/core';
import {Body} from "./Body";
import {SnackbarProvider} from 'notistack';
import React, {FC} from "react";
import WalletWrapper from "./components/SnackbarWalletKit";
import {Footer} from "./Footer";
import './App.css';

export const BREAKPOINT_SIZES = [576, 780, 992, 1200] as const;

const maxMediaQueries = BREAKPOINT_SIZES.map(
    (bp) => `@media (max-width: ${bp}px)`
);

export const breakpoints = {
    mobile: maxMediaQueries[0],
    tablet: maxMediaQueries[1],
    medium: maxMediaQueries[2],
};

const theme = createTheme({
    palette: {
        type: 'light'
    },
    props: {
        MuiInput: { inputProps: { spellCheck: 'false' } }
    },
    overrides: {
        MuiChip: {
            root: {
                borderRadius: 3,
            },
        },
        MuiButtonBase: {
            root: {
                justifyContent: 'flex-start',
            },
        },
        MuiButton: {
            root: {
                textTransform: undefined,
                padding: '12px 16px',
            },
            startIcon: {
                marginRight: 8,
            },
            endIcon: {
                marginLeft: 8,
            },
        },
    },
});

const App: FC = () => {
  return (
    <ThemeProvider theme={theme}>
        <SnackbarProvider maxSnack={3}>
            <WalletWrapper>
                <Body />
                <Footer/>
            </WalletWrapper>
        </SnackbarProvider>
    </ThemeProvider>
  );
};

export default App;
