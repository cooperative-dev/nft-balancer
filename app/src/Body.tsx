import {css} from "@emotion/react";
import styled from "@emotion/styled";
import {ConnectWalletButton} from "@gokiprotocol/walletkit";
import {useConnectedWallet, useSolana} from "@saberhq/use-solana";
import {lighten} from "polished";
import React from "react";
import SellGrid from "./components/sell/SellGrid";
import NetworkSwitch from "./components/NetworkSwitch";
import StatsInfo from "./components/StatsInfo";
import Admin from "./components/Admin";

export const Body: React.FC = () => {
  const { disconnect, providerMut } = useSolana();
  const wallet = useConnectedWallet();

  return (
    <AppWrapper>
        <div
            css={css`
                position: absolute;
                top: 20px;
                right: 20px;
                display: flex;
                flex-direction: column;
                align-items: center;
            `}
        >
            {wallet ? (
                <Button onClick={disconnect}>Disconnect</Button>
            ) : (
                <ConnectWalletButton />
            )}
            <NetworkSwitch/>
        </div>
        <h1 />
        <p>
            Cooperative.dev DAO
        </p>
        <small>
            NFT sell-only balancer
        </small>
        <Info>
            Balancer is a bit of a misnomer here. 22.5% of the royalties from the NFT sales are sent to this pool, and offer a measure of instant liquidity for those wishing to sell their DAO membership. The price does not change as a result of NFTs being sold into the balancer, though it does increase with royalties from sales.
        </Info>

        <Admin/>

        {providerMut &&
            <StatsInfo
                providerMut={providerMut}
            />
        }

        {wallet ? (
            <SellGrid />
        ) : (
            <WalletInfo>
                <p>Connect your wallet (top right) to begin.</p>
            </WalletInfo>
        )}
    </AppWrapper>
  );
};

const AppWrapper = styled.div`
  background-color: #282c34;
  min-height: calc(100vh - 21px);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: top;
  font-size: calc(10px + 2vmin);
  color: white;
  text-align: center;
`;

const WalletInfo = styled.div`
  background: ${lighten(0.1, "#282c34")};
  padding: 12px 24px;
  border-radius: 8px;
  font-size: 16px;
  text-align: left;
  align-items: center;
  text-align: center;
  max-width: 300px;
  margin: 100px;
`;

const Button = styled.button`
  display: flex;
  align-items: center;
  gap: 12px;

  cursor: pointer;
  border: none;
  outline: none;
  height: 40px;
  mix-blend-mode: normal;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
  border-radius: 4px;
  padding: 0 12px;

  background: #000;
  color: #fff;
  &:hover {
    background: ${lighten(0.1, "#000")};
  }

  font-weight: bold;
  font-size: 16px;
  line-height: 20px;
`;

const Info = styled.div`
  background: ${lighten(0.1, "#282c34")};
  padding: 12px 24px;
  border-radius: 8px;
  font-size: 16px;
  text-align: left;
  margin: 20px;
  max-width: 600px;
`;