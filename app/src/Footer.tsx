import styled from "@emotion/styled";
import React from "react";

export const Footer: React.FC = () => {
    return (
        <FooterDiv>
            <a target='_blank' href={'https://twitter.com/cooperative_dev'}>
                @cooperative_dev
            </a>
        </FooterDiv>
    );
};

const FooterDiv = styled.div`
    a:link, a:visited {
        text-decoration: none;
        color: inherit;
    }
    color: white;
    text-align: center;
    background-color: grey;
    height: 21px;
`;