import * as React from "react";
import InitializeTreasury from "./initialize/InitializeTreasury";
import AddMints from "./initialize/AddMints";
import FreezeAddingMints from "./initialize/FreezeAddingMints";
import WithdrawGrid from "./withdraw/WithdrawGrid";
import {Divider} from "antd";

export default function Admin() {
    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
        return (
            <>
                <Divider style={{backgroundColor: "white"}}>Admin controls -- only visible in development mode</Divider>
                <InitializeTreasury/>
                <AddMints/>
                <FreezeAddingMints/>
                <WithdrawGrid/>
                <Divider style={{backgroundColor: "white"}}>End admin controls</Divider>
            </>
        );
    } else {
        return <></>
    }
}