import {Switch as AntSwitch} from 'antd';
import * as React from "react";
import {useSolana} from "@saberhq/use-solana";

export default function NetworkSwitch() {
    const { network, setNetwork } = useSolana();

    const [networkString, setNetworkString] = React.useState("mainnet-beta");

    const handleChange = () => {
        if (networkString == 'mainnet-beta'){
            setNetwork("devnet");
            setNetworkString("devnet");
        } else {
            setNetwork("mainnet-beta");
            setNetworkString("mainnet-beta");
        }
    };

    return (
        <>
            <AntSwitch style={{marginTop: "20px"}} defaultChecked onChange={handleChange} />
            <small style={{fontSize: "12px"}}>{network}</small>
        </>
    );
}