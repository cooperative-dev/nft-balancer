import {useSnackbar, withSnackbar} from 'notistack';
import {WalletKitProvider} from "@gokiprotocol/walletkit";
import React from "react";
import {ErrorLevel, UseSolanaError} from "@saberhq/use-solana";

const WalletWrapper: React.FC = ({ children }) => {

    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const handleConnect = () => {
        enqueueSnackbar('Connected');
    };

    const handleDisconnect = () => {
        enqueueSnackbar('Disconnected');
    };

    const handleError = (err: UseSolanaError) => {
        if (err.level === ErrorLevel.WARN) {
            enqueueSnackbar(err, { variant: 'warning' })
        } else {
            enqueueSnackbar(err, { variant: 'error' })
        }
    };

    return (
        <WalletKitProvider
            defaultNetwork="mainnet-beta"
            app={{
                name: "NFT Balancer",
            }}
            onConnect={handleConnect}
            onDisconnect={handleDisconnect}
            onError={handleError}
            networkConfigs={
                {
                    "mainnet-beta": {
                        name: "Mainnet Beta",
                        endpoint: "https://frosty-wispy-fire.solana-mainnet.quiknode.pro/",
                    },
                    devnet: {
                        name: "Devnet",
                        endpoint: "https://summer-polished-dream.solana-devnet.quiknode.pro/8323a50f2ddd61aa4205ee2ad80532472090b346/",
                    }
                }
            }
        >
            {children}
        </WalletKitProvider>
    )
}

export default withSnackbar(WalletWrapper);