import React, {useEffect, useState} from "react";
import styled from "@emotion/styled";
import {lighten} from "polished";
import * as anchor from "@project-serum/anchor";
import {createProvider, getProgram, getTreasuryPDA} from "../config";
import {Provider} from "@saberhq/solana-contrib";
import {LAMPORTS_PER_SOL} from '@solana/web3.js';

export interface StatsInfoProps {
    providerMut: Provider;
}

export default function StatsInfo(props: StatsInfoProps) {
    const { providerMut } = props;

    const provider = createProvider(providerMut);
    anchor.setProvider(provider);
    const program = getProgram(provider);

    const [balance, setBalance] = useState(0.0);
    const [collectionSize, setCollectionSize] = useState(0);
    const [nftsHeld, setNftsHeld] = useState(0);

    useEffect(() => {
        async function getData() {
            try {
                let treasuryPDA = await getTreasuryPDA(provider)
                let treasuryPDAAccount = await program.account.treasuryData.fetch(
                    treasuryPDA
                );
                setBalance(await provider.connection.getBalance(treasuryPDAAccount.treasury));
                setCollectionSize(treasuryPDAAccount.nftCollectionCount.toNumber())
                setNftsHeld(treasuryPDAAccount.nftHeldCount.toNumber())
            } catch (e) {

            }
        }
        getData()
    }, [providerMut, provider, program]);

    return (
        <Info>
            Treasury balance: ◎{Math.round(10000 * balance/LAMPORTS_PER_SOL) / 10000}
            <br />
            Collection total: {collectionSize}
            <br />
            NFTs held in balancer: {nftsHeld}
            <br />
            Price: ◎{Math.round(10000 * 0.995 * (balance/(collectionSize-nftsHeld))/LAMPORTS_PER_SOL) / 10000}
        </Info>
    );
}

const Info = styled.div`
  background: ${lighten(0.1, "#282c34")};
  padding: 12px 24px;
  border-radius: 8px;
  font-size: 16px;
  text-align: left;
  margin: 10px;
  max-width: 600px;
`;