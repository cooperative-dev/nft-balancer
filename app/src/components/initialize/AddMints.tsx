import {PublicKey} from "@solana/web3.js";
import React, {useState} from "react";
import {useSolana} from "@saberhq/use-solana";
import {rpcAddValidMint} from "../../rpc/balancer";
import {Input} from 'antd';
import Button from "@mui/material/Button";
import invariant from "tiny-invariant";
import CircularProgress from "@material-ui/core/CircularProgress";
import {notify} from "../../utils/notifications";

const { TextArea } = Input;

const AddMints = () => {
    const { providerMut } = useSolana();

    const [json, setJson] = useState('');
    const [loading, setLoading] = useState(false);

    const exampleJson = '{"mints": ["5ZUFEvd97ckP4nYScbuYvsfpEy7CCVj98EeFsBSo2J58", "7epnMxuaBY8gZduNJMgh6DZsaeSATG9e4dfKqJNEwmN3"]}';

    return (
        <div>
            <div style={{fontSize: "16px"}}>{exampleJson}</div>
            <TextArea
                rows={4}
                onChange={((e) => setJson(e.target.value))}
            />
            <Button size="large" variant="outlined"
                onClick={
                    async () => {
                        let obj: { mints: string[] } = JSON.parse(json);

                        console.log('adding', obj.mints.length, 'mints')

                        for (const mint of obj.mints) {
                            let i = obj.mints.indexOf(mint);
                            setLoading(true)
                            let tx_sig: string = ""
                            try {
                                invariant(providerMut, "providerMut");
                                tx_sig = await rpcAddValidMint(
                                    providerMut,
                                    new PublicKey(mint),
                                );
                            } catch (e) {
                                notify({
                                    message: "Error adding mint #" + i + ": " + JSON.stringify(e),
                                    type: "error"
                                })
                            } finally {
                                setLoading(false)
                            }
                            if (tx_sig) {
                                console.log(tx_sig)
                                console.log("https://solscan.io/tx/" + tx_sig)
                                notify({
                                    message: "Added mint #" + i,
                                    txid: tx_sig,
                                    type: "success"
                                })
                            }
                        }
                    }
                }
            >
                Add mints
                {loading &&
                    <CircularProgress
                        size={20}
                    />
                }
            </Button>
        </div>
    );
}

export default AddMints;