import React, {useState} from "react";
import {useConnectedWallet, useSolana} from "@saberhq/use-solana";
import {rpcFreezeAddingMints} from "../../rpc/balancer";
import Button from "@mui/material/Button";
import invariant from "tiny-invariant";
import CircularProgress from "@material-ui/core/CircularProgress";
import {notify} from "../../utils/notifications";

const FreezeAddingMints = () => {
    const { providerMut } = useSolana();

    const [loading, setLoading] = useState(false);

    return (
        <div>
            <Button size="large" variant="outlined"
                onClick={
                    async () => {
                        setLoading(true)
                        let tx_sig: string = ""
                        try {
                            invariant(providerMut, "providerMut");
                            tx_sig = await rpcFreezeAddingMints(
                                providerMut,
                            );
                        } catch (e) {
                            notify({
                                message: "Error w/ freeze adding mints" + JSON.stringify(e),
                                type: "error"
                            })
                        } finally {
                            setLoading(false)
                        }
                        if (tx_sig) {
                            console.log(tx_sig)
                            console.log("https://solscan.io/tx/" + tx_sig)
                            notify({
                                message: "Freeze adding mints",
                                txid: tx_sig,
                                type: "success"
                            })
                        }
                    }
                }
            >
                Freeze Adding Mints
                {loading &&
                    <CircularProgress
                        size={20}
                    />
                }
            </Button>
        </div>
    );
}

export default FreezeAddingMints;