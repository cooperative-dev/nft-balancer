import React, {useState} from "react";
import {useSolana} from "@saberhq/use-solana";
import {rpcInitializeTreasury} from "../../rpc/balancer";
import Button from "@mui/material/Button";
import invariant from "tiny-invariant";
import CircularProgress from "@material-ui/core/CircularProgress";
import {notify} from "../../utils/notifications";

const InitializeTreasury = () => {
    const { providerMut } = useSolana();

    const [loading, setLoading] = useState(false);

    return (
        <div>
            <Button size="large" variant="outlined"
                onClick={
                    async () => {
                        setLoading(true)
                        let tx_sig: string = ""
                        try {
                            invariant(providerMut, "providerMut");
                            tx_sig = await rpcInitializeTreasury(
                                providerMut,
                            );
                        } catch (e) {
                            notify({
                                message: "Error initializing treasury: " + JSON.stringify(e),
                                type: "error"
                            })
                        } finally {
                            setLoading(false)
                        }
                        if (tx_sig) {
                            console.log(tx_sig)
                            console.log("https://solscan.io/tx/" + tx_sig)
                            notify({
                                message: "Initialized treasury",
                                txid: tx_sig,
                                type: "success"
                            })
                        }
                    }
                }
            >
                Initialize Treasury
                {loading &&
                    <CircularProgress
                        size={20}
                    />
                }
            </Button>
        </div>
    );
}

export default InitializeTreasury;