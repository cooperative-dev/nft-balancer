import * as React from 'react';
import Button from '@mui/material/Button';
import {Metadata} from "../../tools/metadata";
import invariant from "tiny-invariant";
import {Provider} from "@saberhq/solana-contrib"
import CircularProgress from '@material-ui/core/CircularProgress'
import {rpcSellNft} from "../../rpc/balancer";
import {notify} from "../../utils/notifications";

export interface SellNFTButtonProps {
    metadata: Metadata;
    providerMut: Provider;
}

export default function SellButton(props: SellNFTButtonProps) {
    const {metadata, providerMut} = props;
    const [loading, setLoading] = React.useState(false);

    return (
        <Button size="large" variant="outlined"
                onClick={
                    async () => {
                        setLoading(true)
                        let tx_sig: string = ""
                        try {
                            invariant(providerMut, "providerMut");
                            tx_sig = await rpcSellNft(
                                providerMut,
                                metadata,
                            );
                        } catch (e) {
                            notify({
                                message: "Error selling NFT: " + JSON.stringify(e),
                                type: "error"
                            })
                        } finally {
                            setLoading(false)
                        }
                        if (tx_sig) {
                            console.log(tx_sig)
                            console.log("https://solscan.io/tx/" + tx_sig)
                            notify({
                                message: "Sold NFT",
                                txid: tx_sig,
                                type: "success"
                            })
                        }
                    }}
        >
            Sell NFT
            {loading &&
                <CircularProgress
                    size={20}
                />
            }
        </Button>
    );
}
