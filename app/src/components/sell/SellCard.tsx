import { Box, Card, CardContent, CardMedia, Chip,Typography } from "@material-ui/core";
import { Skeleton } from '@material-ui/lab';
import React, { FC, useEffect, useState } from "react";
import { IMetadataExtension, Metadata } from "../../tools/metadata";
import { Provider } from "@saberhq/solana-contrib";
import SellButton from "./SellButton";

export interface SellCardProps {
    metadata: Metadata;
    providerMut: Provider;
}

const SellCard = (props: SellCardProps) => {
    const { metadata, providerMut } = props;
    const [metadataExtension, setMetadataExtension] = useState<IMetadataExtension>()

    useEffect(() => {
        async function fetchMetadataExtension() {
            const newMetadataExtension = await (await fetch(metadata.data.uri))
                .json() as IMetadataExtension;
            setMetadataExtension(newMetadataExtension);
        }
        fetchMetadataExtension();
    }, [metadata]);

    return (
        <Card
            variant="outlined"
            style={{height: "100%", display: "flex", padding: "10px", minWidth: "125px"}}
        >
            <CardContent style={{alignItems: "stretch", flexGrow: 2, padding: "0px"}}>
                <div style={{display: "flex", alignItems: "stretch", height: "100%", justifyContent: "flex-between", flexDirection: "column"}}>
                    <div style={{display: "inline-block", marginTop: "10px"}}>
                        {metadataExtension
                            ? <CardMedia
                                component="img"
                                style={{width: "auto", height: "20vh", margin: "auto"}}
                                src={metadataExtension?.image}
                            />
                            : <Skeleton variant="rect" style={{height: "30vh"}} />
                        }
                    </div>
                    <div>
                        <Typography variant="h6">
                            <b>{metadata.data.name}</b>
                        </Typography>
                    </div>
                    <div>
                        <SellButton
                            metadata={metadata}
                            providerMut={providerMut}
                        />
                    </div>
                </div>
            </CardContent>
        </Card>
    );
};

export default SellCard;
