import {Box, Grid} from "@material-ui/core";
import {TOKEN_PROGRAM_ID} from "@solana/spl-token";
import {PublicKey} from "@solana/web3.js";
import React, {useEffect, useMemo, useState} from "react";
import SellCard from "./SellCard";
import {decodeMetadata, getMetadataAddress, Metadata} from "../../tools/metadata";
import {useConnectedWallet, useSolana} from "@saberhq/use-solana";
import {Provider} from "@saberhq/solana-contrib";
import {NFT_UPDATE_AUTHORITY} from "../../config";

type TokenAndMintAccounts = {
    tokenAccount: PublicKey,
    mintAccount: PublicKey
}

const getPotentialNFTs = async (providerMut: Provider): Promise<TokenAndMintAccounts[]> => {
    const parsedTokenAccounts = (await providerMut.connection.getParsedTokenAccountsByOwner(
        providerMut.wallet.publicKey,
        {programId: TOKEN_PROGRAM_ID}
    )).value;

    return parsedTokenAccounts
        .map(({pubkey, account}) => {
            const info = account.data.parsed.info;
            if(info.tokenAmount.uiAmount === 1 && info.tokenAmount.decimals === 0) {
                return {
                    tokenAccount: pubkey,
                    mintAccount: new PublicKey(info.mint as string),
                };
            }
            return;
        })
        .filter(Boolean) as TokenAndMintAccounts[];
}

const getMetadatas = async (providerMut: Provider, tokenAndMints: TokenAndMintAccounts[]): Promise<Metadata[]> => {
    const metadataAddresses = await Promise.all(
        tokenAndMints.map(tokenAndMint => getMetadataAddress(tokenAndMint.mintAccount))
    );
    const metadataAccountInfos = await providerMut.connection.getMultipleAccountsInfo(metadataAddresses);
    let metadatasOrNothing: (Metadata | undefined)[] = [];
    for (const metadataAccountInfo of metadataAccountInfos) {
        if (metadataAccountInfo) {
            metadatasOrNothing.push(decodeMetadata(metadataAccountInfo.data));
        } else {
            metadatasOrNothing.push(undefined)
        }
    }

    let metadatas: Metadata[] = []

    metadatasOrNothing.map((metadata, i) => {
        if (metadata){
            metadata.tokenAccount = tokenAndMints[i].tokenAccount.toString()
            metadatas.push(metadata)
        }
    });

    return metadatas
};

type NFTMetadatas = {
    nfts: Metadata[]
};

const filterMetadatas = (metadatas: Metadata[]): NFTMetadatas => {
    const nfts: Metadata[] = [];
    for (const metadata of metadatas) {
        if (metadata.updateAuthority == NFT_UPDATE_AUTHORITY.toBase58()){
            nfts.push(metadata);
        }
    }

    return {
        nfts
    };
};

const SellGrid = () => {
    const { providerMut } = useSolana();
    const wallet = useConnectedWallet();

    const [nftMetadatas, setNFTMetadatas] = useState<NFTMetadatas>();

    useEffect(() => {
        setNFTMetadatas(undefined);
        if (!wallet?.publicKey) return;
        if (!providerMut?.connection) return;
        getPotentialNFTs(providerMut)
            .then(potentialNFTs => getMetadatas(providerMut, potentialNFTs))
            .then(filterMetadatas)
            .then(setNFTMetadatas);
    }, [providerMut, wallet])

    const NFTs = useMemo(() => {
        if (!nftMetadatas) return;
        return [
            ...nftMetadatas.nfts
        ];
    }, [nftMetadatas]);

    return (
        <Box style={{margin: "20px"}}>
            <Grid
                container
                justifyContent="center"
                alignItems="stretch"
                direction="row"
                spacing={2}
            >
                {providerMut?.connection
                    ? NFTs?.map(nft =>
                        <Grid item key={nft.mint}
                              xs={6} md={4} lg={3} xl={2}
                              style={{minWidth: "325px"}}
                        >
                            <SellCard
                                metadata={nft}
                                providerMut={providerMut}
                            />
                        </Grid>
                    )
                    : null
                }
            </Grid>
        </Box>
    );
}

export default SellGrid;