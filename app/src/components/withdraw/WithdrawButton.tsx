import * as React from 'react';
import Button from '@mui/material/Button';
import invariant from "tiny-invariant";
import {Provider} from "@saberhq/solana-contrib"
import CircularProgress from '@material-ui/core/CircularProgress'
import {rpcWithdrawNft} from "../../rpc/balancer";
import {notify} from "../../utils/notifications";
import {WithdrawDataPlus} from "../../config";

export interface WithdrawButtonProps {
    data: WithdrawDataPlus;
    providerMut: Provider;
}

export default function WithdrawButton(props: WithdrawButtonProps) {
    const { data, providerMut } = props;
    const [loading, setLoading] = React.useState(false);

    return (
        <div>
            <Button size="large" variant="outlined"
                    onClick={
                        async () => {
                            setLoading(true)
                            let tx_sig: string = ""
                            try {
                                invariant(providerMut, "providerMut");
                                tx_sig = await rpcWithdrawNft(
                                    providerMut,
                                    data
                                );
                            } catch (e) {
                                notify({
                                    message: "Error withdrawing NFT: " + JSON.stringify(e),
                                    type: "error"
                                })
                            } finally {
                                setLoading(false)
                            }
                            if (tx_sig) {
                                console.log(tx_sig)
                                console.log("https://solscan.io/tx/" + tx_sig)
                                notify({
                                    message: "Withdrew NFT",
                                    txid: tx_sig,
                                    type: "success"
                                })
                            }
                        }}
            >
                Withdraw NFT
                {loading &&
                    <CircularProgress
                        size={20}
                    />
                }
            </Button>
        </div>
    );
}