import {Box, Card, CardContent, CardMedia, Chip, Typography} from "@material-ui/core";
import {Skeleton} from '@material-ui/lab';
import React, {useEffect, useState} from "react";
import {IMetadataExtension} from "../../tools/metadata";
import {Provider} from "@saberhq/solana-contrib"
import WithdrawButton from "./WithdrawButton";
import {WithdrawDataPlus} from "../../config";

export interface WithdrawNFTProps {
    data: WithdrawDataPlus;
    providerMut: Provider;
}

const WithdrawCard = (props: WithdrawNFTProps) => {
    const { data, providerMut } = props;
    const [metadataExtension, setMetadataExtension] = useState<IMetadataExtension>()

    useEffect(() => {
        async function fetchMetadataExtension() {
            const newMetadataExtension = await (await fetch(data.metadata.data.uri))
                .json() as IMetadataExtension;
            setMetadataExtension(newMetadataExtension);
        }
        fetchMetadataExtension();
    }, [data]);

    return (
        <Card
            variant="outlined"
            style={{height: "100%", display: "flex", padding: "0px"}}
        >
            <CardContent style={{alignItems: "stretch", flexGrow: 2, padding: "0px"}}>
                <div style={{display: "flex", alignItems: "stretch", height: "100%", justifyContent: "flex-between", flexDirection: "column"}}>
                    <div style={{display: "inline-block", marginTop: "5px"}}>
                        {metadataExtension
                            ? <CardMedia
                                component="img"
                                style={{width: "auto", height: "20vh", margin: "auto"}}
                                src={metadataExtension?.image}
                            />
                            : <Skeleton variant="rect" style={{height: "30vh"}} />
                        }
                    </div>
                    <div style={{display: "inline-block", height: "100%"}}>
                        <Typography variant="h6">
                            <b>{data?.metadata.data.name}</b>
                        </Typography>
                        <Typography variant={"body2"}>
                            {metadataExtension?.description}
                        </Typography>
                        <Box m={1} style={{marginBottom: "0px"}}>
                            {metadataExtension?.attributes?.map((attribute) =>
                                <Box m={0.5} display="inline-block" key={attribute.trait_type}>
                                    <Chip
                                        size="small"
                                        component="div"
                                        style={{height: "100%"}}
                                        label={(
                                            <Box m={0.5}>
                                                <Typography variant={"body2"}>
                                                    {attribute.trait_type}
                                                </Typography>
                                                <Typography variant={"body2"} color="textSecondary">
                                                    {attribute.value}
                                                </Typography>
                                            </Box>
                                        )}
                                    />
                                </Box>
                            )}
                        </Box>
                    </div>
                    <div style={{display: "inline-block", margin: "0 auto"}}>
                        <WithdrawButton
                            data={data}
                            providerMut={providerMut}
                        />
                    </div>
                </div>
            </CardContent>
        </Card>
    );
};

export default WithdrawCard;
