import {Box, Grid} from "@material-ui/core";
import React, {useEffect, useMemo, useState} from "react";
import {decodeMetadata, getMetadataAddress, Metadata} from "../../tools/metadata";
import {useConnectedWallet, useSolana} from "@saberhq/use-solana";
import {Provider} from "@saberhq/solana-contrib";
import * as anchor from "@project-serum/anchor";
import WithdrawCard from "./WithdrawCard";
import {
    BALANCER_PROGRAM_ID,
    createProvider,
    getProgram,
    getTreasuryPDA,
    MintCheckData,
    WithdrawData,
    WithdrawDataPlus
} from "../../config";

const getPotentialNftHoldings = async (providerMut: Provider): Promise<WithdrawData[]> => {
    const provider = createProvider(providerMut)
    anchor.setProvider(provider);
    const program = getProgram(provider);

    const programAccounts = (await providerMut.connection.getProgramAccounts(
        BALANCER_PROGRAM_ID
    ));

    const treasuryAccount = await getTreasuryPDA(provider);

    return programAccounts.filter(x => {
            return !x.pubkey.equals(treasuryAccount)
        }).filter(x => {
            try {
                return program.account.mintCheck.coder.accounts.decode("mintCheck", x.account.data) as MintCheckData && true
            } catch (e) {
                return false
            }
        }).map(x => {
            return {
                mintCheckData: program.account.mintCheck.coder.accounts.decode("mintCheck", x.account.data) as MintCheckData,
                mintCheckPda: x.pubkey
            }
        }).filter(x => {
            return x.mintCheckData.isHeld
        })
}

const getMetadatas = async (providerMut: Provider, withdrawData: WithdrawData[]): Promise<WithdrawDataPlus[]> => {
    const metadataAddresses = await Promise.all(
        withdrawData.map(withdrawData => getMetadataAddress(withdrawData.mintCheckData.mint))
    );
    const metadataAccountInfos = await providerMut.connection.getMultipleAccountsInfo(metadataAddresses);
    let metadatasOrNothing: (Metadata | undefined)[] = [];
    for (const metadataAccountInfo of metadataAccountInfos) {
        if (metadataAccountInfo) {
            metadatasOrNothing.push(decodeMetadata(metadataAccountInfo.data));
        } else {
            metadatasOrNothing.push(undefined)
        }
    }

    const provider = createProvider(providerMut)
    anchor.setProvider(provider);
    const program = getProgram(provider);

    const treasuryAccount = await getTreasuryPDA(provider);

    let treasuryData = await program.account.treasuryData.fetch(
        treasuryAccount
    );

    let withdrawDataPlus: WithdrawDataPlus[] = []

    metadatasOrNothing.map((metadata, i) => {
        if (metadata){
            metadata.tokenAccount = withdrawData[i].mintCheckPda.toString()

            const fullObj: WithdrawDataPlus = {
                treasuryData,
                mintCheckData: withdrawData[i].mintCheckData,
                mintCheckPda: withdrawData[i].mintCheckPda,
                metadata,
            }

            withdrawDataPlus.push(fullObj)
        }
    });

    return withdrawDataPlus
};

type SingleDataObj = {
    array: WithdrawDataPlus[]
};

const sortAndSplitMetadatas = (datas: WithdrawDataPlus[]): SingleDataObj => {
    const nfts: WithdrawDataPlus[] = [];
    for (const data of datas) {
        nfts.push(data);
    }

    return {
        array: nfts
    };
};

const WithdrawGrid = () => {
    const { providerMut } = useSolana();
    const wallet = useConnectedWallet();

    const [nftMetadatas, setNFTMetadatas] = useState<SingleDataObj>();

    useEffect(() => {
        setNFTMetadatas(undefined);
        if (!wallet?.publicKey) return;
        if (!providerMut?.connection) return;
        getPotentialNftHoldings(providerMut)
            .then(fullObjs => getMetadatas(providerMut, fullObjs))
            .then(sortAndSplitMetadatas)
            .then(setNFTMetadatas);
    }, [providerMut, wallet])

    const NFTs = useMemo(() => {
        if (!nftMetadatas) return;
        return [
            ...nftMetadatas.array
        ];
    }, [nftMetadatas]);

    return (
        <Box>
            <Grid
                container
                justifyContent="center"
                alignItems="stretch"
                direction="row"
                spacing={2}
            >
                {providerMut?.connection
                    ? NFTs?.map(nft =>
                        <Grid item key={nft.metadata.mint}
                              xs={6} md={4} lg={3} xl={2}
                              style={{minWidth: "325px"}}
                        >
                            <WithdrawCard
                                data={nft}
                                key={nft.metadata.mint}
                                providerMut={providerMut}
                            />
                        </Grid>
                    )
                    : null
                }
            </Grid>
        </Box>
    );
}

export default WithdrawGrid;