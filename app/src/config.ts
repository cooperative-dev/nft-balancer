import {PublicKey} from "@solana/web3.js";
import {Metadata} from "./tools/metadata";
import {IdlAccounts, Program, Provider as AnchorProvider} from "@project-serum/anchor";
import {Balancer, IDL as BalancerIdl} from "./idls/balancer/balancer";
import Provider from "@project-serum/anchor/src/provider";
import * as anchor from "@project-serum/anchor";
import {Provider as SaberProvider} from "@saberhq/solana-contrib/dist/interfaces";

export const BALANCER_PROGRAM_ID =
    new PublicKey("C93wLCtgqKCzJMXCiWj8CL54aXKAjtGwi7Rc4ggQ11iu")

export const DEV_ROYALTY_ACCOUNT =
    new PublicKey("Ds2vyqB38wawvVX6jEnXt2jeoPcd3spU8LRzwdHKcuZv")

export const NFT_UPDATE_AUTHORITY =
    new PublicKey("FaWBJ6U22uQwV8o1gxmAW4R1WhbA4wMR7BSP4qKnE8Jy")

export const getProgram = function (provider?: Provider): Program<Balancer> {
    return new Program<Balancer>(BalancerIdl, BALANCER_PROGRAM_ID, provider);
}

export type TreasuryData = IdlAccounts<Balancer>['treasuryData']
export type MintCheckData = IdlAccounts<Balancer>['mintCheck']

export type WithdrawData = {
    mintCheckData: MintCheckData,
    mintCheckPda: PublicKey,
}

export type WithdrawDataPlus = {
    treasuryData: TreasuryData,
    mintCheckData: MintCheckData,
    mintCheckPda: PublicKey,
    metadata: Metadata,
}

export const getTreasuryPDA = async (provider: Provider) => {
    const [pda, nonce] = await PublicKey.findProgramAddress(
        [Buffer.from(anchor.utils.bytes.utf8.encode("treasury"))],
        getProgram(provider).programId
    );
    return pda
}

export const getMintCheckPDA = async (provider: Provider, mint: PublicKey) => {
    const [pda, nonce] = await PublicKey.findProgramAddress(
        [mint.toBuffer()],
        getProgram(provider).programId
    );
    return pda
}

export const createProvider = (providerMut: SaberProvider): AnchorProvider => {
    return new AnchorProvider(
        providerMut.connection,
        providerMut.wallet,
        providerMut.opts
    )
}