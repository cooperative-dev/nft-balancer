import * as anchor from '@project-serum/anchor';
import {Provider as AnchorProvider} from '@project-serum/anchor';
import {Provider as SaberProvider} from "@saberhq/solana-contrib";
import {Metadata} from "../tools/metadata";
import {Keypair, PublicKey, SystemProgram, TransactionSignature} from "@solana/web3.js";
import {TOKEN_PROGRAM_ID} from "@solana/spl-token";
import {BALANCER_PROGRAM_ID, DEV_ROYALTY_ACCOUNT, getProgram, WithdrawDataPlus} from "../config";

const TokenInstructions = require("@project-serum/serum").TokenInstructions;

export const rpcInitializeTreasury = async (
    providerMut: SaberProvider,
): Promise<TransactionSignature> => {
    const provider = createProvider(providerMut)
    anchor.setProvider(provider);
    const program = getProgram(provider);

    const [treasuryPDA, _bump] = await PublicKey.findProgramAddress(
        [Buffer.from(anchor.utils.bytes.utf8.encode("treasury"))],
        BALANCER_PROGRAM_ID
    );

    const treasuryWallet = Keypair.generate();

    const lamports = await provider.connection.getMinimumBalanceForRentExemption(8);

    return program.rpc.initializeTreasury({
            accounts: {
                authority: provider.wallet.publicKey,
                treasuryData: treasuryPDA,
                treasury: treasuryWallet.publicKey,
                devRoyaltyAccount: DEV_ROYALTY_ACCOUNT,
                systemProgram: SystemProgram.programId,
            },
            instructions: [
                anchor.web3.SystemProgram.createAccount({
                    fromPubkey: provider.wallet.publicKey,
                    newAccountPubkey: treasuryWallet.publicKey,
                    lamports,
                    space: 8,
                    programId: program.programId,
                }),
            ],
            signers: [treasuryWallet]
        }
    )
}

export const rpcAddValidMint = async (
    providerMut: SaberProvider,
    validMint: PublicKey,
): Promise<TransactionSignature> => {
    const provider = createProvider(providerMut)
    anchor.setProvider(provider);
    const program = getProgram(provider);

    const [treasuryPDA, _tbump] = await PublicKey.findProgramAddress(
        [Buffer.from(anchor.utils.bytes.utf8.encode("treasury"))],
        BALANCER_PROGRAM_ID
    );

    const [_mintCheckPDA, _] = await PublicKey.findProgramAddress(
        [validMint.toBuffer()],
        BALANCER_PROGRAM_ID
    );

    return program.rpc.addValidMint({
            accounts: {
                authority: provider.wallet.publicKey,
                mint: validMint,
                mintCheck: _mintCheckPDA,
                treasuryData: treasuryPDA,
                systemProgram: SystemProgram.programId,
            },
        }
    );
}

export const rpcFreezeAddingMints = async (
    providerMut: SaberProvider,
): Promise<TransactionSignature> => {
    const provider = createProvider(providerMut)
    anchor.setProvider(provider);
    const program = getProgram(provider);

    const [treasuryPDA, _tbump] = await PublicKey.findProgramAddress(
        [Buffer.from(anchor.utils.bytes.utf8.encode("treasury"))],
        BALANCER_PROGRAM_ID
    );

    return program.rpc.freezeAddingMints({
            accounts: {
                authority: provider.wallet.publicKey,
                treasuryData: treasuryPDA,
            },
        }
    )
}

export const rpcSellNft = async (
    providerMut: SaberProvider,
    nft: Metadata,
): Promise<TransactionSignature> => {
    const provider = createProvider(providerMut)
    anchor.setProvider(provider);
    const program = getProgram(provider);

    const seller = provider.wallet;
    const nftLocker = Keypair.generate();

    const [treasuryPDA, _tbump] = await PublicKey.findProgramAddress(
        [Buffer.from(anchor.utils.bytes.utf8.encode("treasury"))],
        BALANCER_PROGRAM_ID
    );

    const [mintCheckPDA, _mbump] = await PublicKey.findProgramAddress(
        [new PublicKey(nft.mint).toBuffer()],
        BALANCER_PROGRAM_ID
    );

    let treasuryPDAData = await program.account.treasuryData.fetch(
        treasuryPDA
    );

    const lamports = await provider.connection.getMinimumBalanceForRentExemption(165);

    return program.rpc.sellNft({
        accounts: {
            seller: seller.publicKey,
            sellerDepositTokenAccount: nftLocker.publicKey,
            sellerCurrentTokenAccount: new PublicKey(nft.tokenAccount),
            mint: new PublicKey(nft.mint),
            mintCheck: mintCheckPDA,
            treasuryData: treasuryPDA,
            treasury: treasuryPDAData.treasury,
            devRoyaltyAccount: DEV_ROYALTY_ACCOUNT,
            systemProgram: SystemProgram.programId,
            tokenProgram: TOKEN_PROGRAM_ID,
        },
        instructions: [
            anchor.web3.SystemProgram.createAccount({
                fromPubkey: seller.publicKey,
                newAccountPubkey: nftLocker.publicKey,
                lamports,
                space: 165,
                programId: TOKEN_PROGRAM_ID,
            }),
            TokenInstructions.initializeAccount({
                account: nftLocker.publicKey,
                mint: new PublicKey(nft.mint),
                owner: treasuryPDA,
            }),
        ],
        signers: [nftLocker],
    })
}

export const rpcWithdrawNft = async (
    providerMut: SaberProvider,
    data: WithdrawDataPlus
): Promise<TransactionSignature> => {
    const provider = createProvider(providerMut)
    anchor.setProvider(provider);
    const program = getProgram(provider);

    const withdrawFromTokenAccount = data.mintCheckData.heldTokenAccount;
    const withdrawToTokenAccount = anchor.web3.Keypair.generate();
    const mint = data.mintCheckData.mint
    const mintCheck = data.mintCheckPda

    const [treasuryPDA, _bump] = await PublicKey.findProgramAddress(
        [Buffer.from(anchor.utils.bytes.utf8.encode("treasury"))],
        BALANCER_PROGRAM_ID
    );

    const lamports = await provider.connection.getMinimumBalanceForRentExemption(165);

    return program.rpc.withdrawNft({
            accounts: {
                authority: provider.wallet.publicKey,
                mint,
                mintCheck,
                treasuryData: treasuryPDA,
                withdrawFromTokenAccount,
                withdrawToTokenAccount: withdrawToTokenAccount.publicKey,
                systemProgram: SystemProgram.programId,
                tokenProgram: TOKEN_PROGRAM_ID,
            },
            instructions: [
                anchor.web3.SystemProgram.createAccount({
                    fromPubkey: provider.wallet.publicKey,
                    newAccountPubkey: withdrawToTokenAccount.publicKey,
                    lamports,
                    space: 165,
                    programId: TOKEN_PROGRAM_ID,
                }),
                TokenInstructions.initializeAccount({
                    account: withdrawToTokenAccount.publicKey,
                    mint,
                    owner: provider.wallet.publicKey,
                }),
            ],
            signers: [withdrawToTokenAccount]
        }
    )
}

function createProvider(providerMut: SaberProvider): AnchorProvider {
    return new AnchorProvider(
        providerMut.connection,
        providerMut.wallet,
        providerMut.opts
    )
}