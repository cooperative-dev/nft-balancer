use anchor_lang::prelude::*;
use anchor_spl::token::{self, SetAuthority, Token, TokenAccount, Transfer, Mint, CloseAccount};
use spl_token::instruction::AuthorityType;
use anchor_lang::__private::bytemuck::__core::cell::RefMut;
use anchor_lang::__private::bytemuck::{from_bytes_mut, cast_slice_mut, try_cast_slice_mut};

declare_id!("C93wLCtgqKCzJMXCiWj8CL54aXKAjtGwi7Rc4ggQ11iu");

#[program]
pub mod balancer {
    use super::*;

    const TREASURY_PDA_SEED: &[u8] = b"treasury";

    const DEV_ROYALTY_NUMERATOR: u64 = 50;
    const DEV_ROYALTY_DENOMINATOR: u64 = 10000;

    pub fn initialize_treasury(ctx: Context<InitializeTreasury>,
    ) -> ProgramResult {
        assert!(!ctx.accounts.treasury_data.treasury_initialized);

        ctx.accounts.treasury_data.treasury_initialized  = true;
        ctx.accounts.treasury_data.paused                = true;
        ctx.accounts.treasury_data.authority             = *ctx.accounts.authority.key;
        ctx.accounts.treasury_data.treasury              = *ctx.accounts.treasury.to_account_info().key;
        ctx.accounts.treasury_data.dev_royalty_account   = *ctx.accounts.dev_royalty_account.to_account_info().key;

        Ok(())
    }

    // TODO: this could be done more efficiently in batches, though probably not worth sweating rn
    pub fn add_valid_mint(ctx: Context<AddValidMint>,
    ) -> ProgramResult {
        assert!(&ctx.accounts.treasury_data.paused);

        // mint_check is initialized with default of: 'is_valid' = false
        // this check ensures that the same mint can be added twice without affecting the count
        if !ctx.accounts.mint_check.is_valid {
            ctx.accounts.mint_check.is_valid = true;
            ctx.accounts.mint_check.mint = *ctx.accounts.mint.to_account_info().key;
            ctx.accounts.treasury_data.nft_collection_count += 1;
        }

        Ok(())
    }

    pub fn freeze_adding_mints(ctx: Context<FreezeAddingMints>,
    ) -> ProgramResult {
        ctx.accounts.treasury_data.paused = false;
        Ok(())
    }

    pub fn sell_nft(ctx: Context<SellNFT>,
    ) -> ProgramResult {
        // make sure that the balancer is not paused, and that all valid mints have been added
        assert!(!&ctx.accounts.treasury_data.paused);

        // validate that the mint exists as a check_mint PDA
        let (mint_check, _bump_seed) = Pubkey::find_program_address(
            &[ctx.accounts.seller_current_token_account.mint.key().as_ref()],
            ctx.program_id
        );

        // here we're just making sure that the mint_check they pass in is the one that corresponds to their token
        assert_eq!(mint_check, ctx.accounts.mint_check.to_account_info().key());

        // transfer nft into deposit token account
        token::transfer(ctx.accounts.into_sell_nft_transfer_context(),1)?;

        // close the empty token account, and return funds to seller
        token::close_account(ctx.accounts.into_close_old_token_account_sell());

        // figure out how much of the treasury should be allocated per sale
        let circulating_nfts = ctx.accounts.treasury_data.nft_collection_count - ctx.accounts.treasury_data.nft_held_count;
        let treasury_balance = ctx.accounts.treasury.to_account_info().lamports();

        let sale_price = treasury_balance.checked_div(circulating_nfts).unwrap();

        let dev_royalty = sale_price
            .checked_mul(DEV_ROYALTY_NUMERATOR).unwrap()
            .checked_div(DEV_ROYALTY_DENOMINATOR).unwrap();

        let proceeds = sale_price.checked_sub(dev_royalty).unwrap();

        // ship sol
        **ctx.accounts.treasury
            .to_account_info().try_borrow_mut_lamports()? -= &sale_price;

        **ctx.accounts.dev_royalty_account
            .to_account_info().try_borrow_mut_lamports()? += &dev_royalty;

        **ctx.accounts.seller
            .to_account_info().try_borrow_mut_lamports()? += &proceeds;

        // increment nft_held_count
        ctx.accounts.treasury_data.nft_held_count += 1;

        // update mint_check params
        ctx.accounts.mint_check.is_held = true;
        ctx.accounts.mint_check.held_token_account = ctx.accounts.seller_deposit_token_account.to_account_info().key();

        Ok(())
    }

    pub fn withdraw_nft(ctx: Context<WithdrawNFT>,
    ) -> ProgramResult {
        let (_pda, bump_seed) = Pubkey::find_program_address(&[TREASURY_PDA_SEED], ctx.program_id);
        let seeds = &[&TREASURY_PDA_SEED[..], &[bump_seed]];

        // transfer token to the newly created withdrawer token account
        token::transfer(
            ctx.accounts
                .into_withdraw_nft_context()
                .with_signer(&[&seeds[..]]),
            1,
        )?;

        // close the empty token account, and return funds to withdrawer
        token::close_account(
            ctx.accounts
                .into_close_old_token_account_withdraw()
                .with_signer(&[&seeds[..]]),
        )?;

        // increment nft_held_count
        ctx.accounts.treasury_data.nft_held_count -= 1;
        ctx.accounts.mint_check.is_held = false;

        Ok(())
    }
}

#[derive(Accounts)]
pub struct InitializeTreasury<'info> {
    #[account(signer)]
    pub authority: AccountInfo<'info>,
    #[account(
        init,
        seeds = [b"treasury".as_ref()],
        bump,
        payer = authority,
    )]
    pub treasury_data: Account<'info, TreasuryData>,
    pub treasury: AccountInfo<'info>,
    pub dev_royalty_account: AccountInfo<'info>,
    pub system_program: Program<'info, System>,
}

#[derive(Accounts)]
pub struct AddValidMint<'info> {
    #[account(
        signer,
        constraint = treasury_data.authority == authority.to_account_info().key()
    )]
    pub authority: AccountInfo<'info>,
    pub mint: Account<'info, Mint>,
    #[account(
        init,
        seeds = [mint.key().as_ref()],
        bump,
        payer = authority,
    )]
    pub mint_check: Account<'info, MintCheck>,
    #[account(mut)]
    pub treasury_data: Account<'info, TreasuryData>,
    pub system_program: Program<'info, System>,
}

#[derive(Accounts)]
pub struct FreezeAddingMints<'info> {
    #[account(
        signer,
        constraint = treasury_data.authority == authority.to_account_info().key()
    )]
    pub authority: AccountInfo<'info>,
    #[account(mut)]
    pub treasury_data: Account<'info, TreasuryData>,
}

#[derive(Accounts)]
pub struct SellNFT<'info> {
    #[account(signer)]
    pub seller: AccountInfo<'info>,
    #[account(mut)]
    pub seller_deposit_token_account: Account<'info, TokenAccount>,
    #[account(
        mut,
        constraint = seller_current_token_account.amount == 1
    )]
    pub seller_current_token_account: Account<'info, TokenAccount>,
    #[account(
        constraint = seller_current_token_account.mint == mint.to_account_info().key()
    )]
    pub mint: Account<'info, Mint>,
    #[account(
        mut,
        constraint = mint_check.is_valid == true,
        constraint = mint_check.mint == mint.to_account_info().key()
    )]
    pub mint_check: Account<'info, MintCheck>,
    #[account(mut)]
    pub treasury_data: Account<'info, TreasuryData>,
    #[account(mut)]
    pub treasury: AccountInfo<'info>,
    #[account(
        mut,
        constraint = dev_royalty_account.to_account_info().key() == treasury_data.dev_royalty_account
    )]
    pub dev_royalty_account: AccountInfo<'info>,
    pub system_program: Program<'info, System>,
    pub token_program: Program<'info, Token>,
}

#[derive(Accounts)]
pub struct WithdrawNFT<'info> {
    #[account(
        signer,
        constraint = treasury_data.authority == authority.to_account_info().key()
    )]
    pub authority: AccountInfo<'info>,
    pub mint: Account<'info, Mint>,
    #[account(
        mut,
        constraint = mint_check.is_valid == true,
        constraint = mint_check.is_held == true,
        constraint = mint_check.mint == mint.to_account_info().key()
    )]
    pub mint_check: Account<'info, MintCheck>,
    #[account(mut)]
    pub treasury_data: Account<'info, TreasuryData>,
    #[account(mut)]
    pub withdraw_from_token_account: Account<'info, TokenAccount>,
    #[account(mut)]
    pub withdraw_to_token_account: Account<'info, TokenAccount>,
    pub system_program: Program<'info, System>,
    pub token_program: Program<'info, Token>,
}

#[account]
pub struct TreasuryData {
    pub treasury_initialized: bool,
    pub paused: bool,
    pub authority: Pubkey,
    pub treasury: Pubkey,
    pub nft_held_count: u64,
    pub nft_collection_count: u64,
    pub dev_royalty_account: Pubkey,
}

impl Default for TreasuryData {
    fn default() -> Self {
        TreasuryData {
            treasury_initialized: false,
            paused: true,
            authority: Pubkey::default(),
            treasury: Pubkey::default(),
            nft_held_count: 0u64,
            nft_collection_count: 0u64,
            dev_royalty_account: Pubkey::default(),
        }
    }
}

#[account]
pub struct MintCheck {
    pub is_valid: bool,
    pub mint: Pubkey,
    pub is_held: bool,
    pub held_token_account: Pubkey,
}

impl Default for MintCheck {
    fn default() -> Self {
        MintCheck{
            is_valid: false,
            mint: Pubkey::default(),
            is_held: false,
            held_token_account: Pubkey::default(),
        }
    }
}

impl<'info> From<&mut SellNFT<'info>> for CpiContext<'_, '_, '_, 'info, SetAuthority<'info>> {
    fn from(accounts: &mut SellNFT<'info>) -> Self {
        let cpi_accounts = SetAuthority {
            account_or_mint: accounts
                .seller_deposit_token_account
                .to_account_info()
                .clone(),
            current_authority: accounts.seller.clone(),
        };
        let cpi_program = accounts.token_program.to_account_info();
        CpiContext::new(cpi_program, cpi_accounts)
    }
}

impl<'info> SellNFT<'info> {
    fn into_sell_nft_transfer_context(&self) -> CpiContext<'_, '_, '_, 'info, Transfer<'info>> {
        let cpi_accounts = Transfer {
            from: self.seller_current_token_account.to_account_info().clone(),
            to: self.seller_deposit_token_account.to_account_info().clone(),
            authority: self.seller.clone(),
        };
        let cpi_program = self.token_program.to_account_info();
        CpiContext::new(cpi_program, cpi_accounts)
    }
}

impl<'info> SellNFT<'info> {
    fn into_close_old_token_account_sell(&self) -> CpiContext<'_, '_, '_, 'info, CloseAccount<'info>> {
        let cpi_accounts = CloseAccount {
            account: self.seller_current_token_account.to_account_info().clone(),
            destination: self.seller.to_account_info().clone(),
            authority: self.seller.to_account_info().clone(),
        };
        let cpi_program = self.token_program.to_account_info();
        CpiContext::new(cpi_program, cpi_accounts)
    }
}

impl<'info> From<&mut WithdrawNFT<'info>> for CpiContext<'_, '_, '_, 'info, SetAuthority<'info>> {
    fn from(accounts: &mut WithdrawNFT<'info>) -> Self {
        let cpi_accounts = SetAuthority {
            account_or_mint: accounts
                .withdraw_from_token_account
                .to_account_info()
                .clone(),
            current_authority: accounts.treasury_data.to_account_info().clone(),
        };
        let cpi_program = accounts.token_program.to_account_info();
        CpiContext::new(cpi_program, cpi_accounts)
    }
}

impl<'info> WithdrawNFT<'info> {
    fn into_withdraw_nft_context(&self) -> CpiContext<'_, '_, '_, 'info, Transfer<'info>> {
        let cpi_accounts = Transfer {
            from: self.withdraw_from_token_account.to_account_info().clone(),
            to: self.withdraw_to_token_account.to_account_info().clone(),
            authority: self.treasury_data.to_account_info().clone(),
        };
        let cpi_program = self.token_program.to_account_info();
        CpiContext::new(cpi_program, cpi_accounts)
    }
}

impl<'info> WithdrawNFT<'info> {
    fn into_close_old_token_account_withdraw(&self) -> CpiContext<'_, '_, '_, 'info, CloseAccount<'info>> {
        let cpi_accounts = CloseAccount {
            account: self.withdraw_from_token_account.to_account_info().clone(),
            destination: self.authority.to_account_info().clone(),
            authority: self.treasury_data.to_account_info().clone(),
        };
        let cpi_program = self.token_program.to_account_info();
        CpiContext::new(cpi_program, cpi_accounts)
    }
}

