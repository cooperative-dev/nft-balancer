import * as anchor from '@project-serum/anchor';
import {Program} from '@project-serum/anchor';
import {Keypair, LAMPORTS_PER_SOL, PublicKey, SystemProgram} from '@solana/web3.js';
import {Token, TOKEN_PROGRAM_ID} from "@solana/spl-token";
import {assert} from "chai";
import {Balancer} from '../target/types/balancer';

const TokenInstructions = require("@project-serum/serum").TokenInstructions;

describe("balancer", () => {
  let provider = anchor.Provider.env();
  anchor.setProvider(provider);
  let program = anchor.workspace.Balancer as Program<Balancer>;

  const mintAuthority = Keypair.generate();
  const devRoyaltyAccount = Keypair.generate();

  let mint: Token = null;
  let validMint: PublicKey = null;
  let treasuryPDA: PublicKey = null;
  let treasury: PublicKey = null;
  let treasuryBump: number = null;
  let nftLockerKey: PublicKey = null;

  it("initialize state", async () => {
    [treasuryPDA, treasuryBump] = await PublicKey.findProgramAddress(
        [Buffer.from(anchor.utils.bytes.utf8.encode("treasury"))],
        program.programId
    );

    const payer = Keypair.generate();
    await provider.connection.confirmTransaction(
        await provider.connection.requestAirdrop(payer.publicKey, 0.1 * LAMPORTS_PER_SOL),
        "confirmed"
    );
    mint = await Token.createMint(
        provider.connection,
        payer,
        mintAuthority.publicKey,
        null,
        0,
        TOKEN_PROGRAM_ID
    );

    await provider.connection.confirmTransaction(
        await provider.connection.requestAirdrop(provider.wallet.publicKey, 0.1 * LAMPORTS_PER_SOL),
        "confirmed"
    );
  });

  it("initialize treasury", async () => {
    const treasuryWallet = Keypair.generate();
    treasury = treasuryWallet.publicKey;

    const lamports = await provider.connection.getMinimumBalanceForRentExemption(8);

    await program.rpc.initializeTreasury({
          accounts: {
            authority: provider.wallet.publicKey,
            treasuryData: treasuryPDA,
            treasury: treasuryWallet.publicKey,
            devRoyaltyAccount: devRoyaltyAccount.publicKey,
            systemProgram: SystemProgram.programId,
          },
          instructions: [
            anchor.web3.SystemProgram.createAccount({
              fromPubkey: provider.wallet.publicKey,
              newAccountPubkey: treasuryWallet.publicKey,
              lamports,
              space: 8,
              programId: program.programId,
            }),
          ],
          signers: [treasuryWallet]
        }
    );

    let treasuryPDAAccount = await program.account.treasuryData.fetch(
        treasuryPDA
    );

    // Check that the values in the escrow account match what we expect.
    assert.ok(treasuryPDAAccount.authority.equals(provider.wallet.publicKey));
    assert.ok(treasuryPDAAccount.treasuryInitialized as Boolean);
    assert.ok(treasuryPDAAccount.paused as Boolean);

    // Give the treasury some SOL
    await provider.connection.confirmTransaction(
        await provider.connection.requestAirdrop(treasury, LAMPORTS_PER_SOL),
        "confirmed"
    );
    const balance = await provider.connection.getBalance(treasury)
    assert.ok(balance >= LAMPORTS_PER_SOL)
  });

  it("add valid mint", async () => {
    const tokenAccount = await mint.createAccount(provider.wallet.publicKey);
    await mint.mintTo(
        tokenAccount,
        mintAuthority.publicKey,
        [mintAuthority],
        1
    );
    let tokenAccountData = await mint.getAccountInfo(
        tokenAccount
    );
    validMint = tokenAccountData.mint;

    const [mintCheckPDA, _] = await PublicKey.findProgramAddress(
        [validMint.toBuffer()],
        program.programId
    );

    await program.rpc.addValidMint({
          accounts: {
            authority: provider.wallet.publicKey,
            mint: validMint,
            mintCheck: mintCheckPDA,
            treasuryData: treasuryPDA,
            systemProgram: SystemProgram.programId,
          },
        }
    );

    let mintCheckPDAData = await program.account.mintCheck.fetch(
        mintCheckPDA
    );

    assert.ok(mintCheckPDAData.isValid as Boolean)
    assert.ok(mintCheckPDAData.mint.equals(tokenAccountData.mint))

    let treasuryPDAAccount = await program.account.treasuryData.fetch(
        treasuryPDA
    );

    assert.ok(treasuryPDAAccount.nftCollectionCount.toNumber() == 1);
    assert.ok(treasuryPDAAccount.nftHeldCount.toNumber() == 0);
  });

  it("freeze adding mints", async () => {
    await program.rpc.freezeAddingMints({
          accounts: {
            authority: provider.wallet.publicKey,
            treasuryData: treasuryPDA,
          },
        }
    );

    let treasuryPDAAccount = await program.account.treasuryData.fetch(
        treasuryPDA
    );

    assert.ok(!(treasuryPDAAccount.paused as Boolean))
  });

  it("check nft sale value", async () => {
    const balance = await provider.connection.getBalance(treasury)

    let treasuryPDAAccount = await program.account.treasuryData.fetch(
        treasuryPDA
    );

    const solPerNft = balance/(
        treasuryPDAAccount.nftCollectionCount.toNumber()
        - treasuryPDAAccount.nftHeldCount.toNumber()
    )

    // console.log('solPerNft:', solPerNft)
  });

  it("sell nft", async () => {
    const seller = Keypair.generate();

    await provider.connection.confirmTransaction(
        await provider.connection.requestAirdrop(seller.publicKey, 0.1 * LAMPORTS_PER_SOL),
        "confirmed"
    );

    const sellerTokenAccount = await mint.createAccount(seller.publicKey);

    await mint.mintTo(
        sellerTokenAccount,
        mintAuthority.publicKey,
        [mintAuthority],
        1
    );

    const lamports = await provider.connection.getMinimumBalanceForRentExemption(165);

    const nftLocker = anchor.web3.Keypair.generate();
    nftLockerKey = nftLocker.publicKey;

    const [mintCheckPDA, _] = await PublicKey.findProgramAddress(
        [validMint.toBuffer()],
        program.programId
    );

    await program.rpc.sellNft({
          accounts: {
            seller: seller.publicKey,
            sellerDepositTokenAccount: nftLocker.publicKey,
            sellerCurrentTokenAccount: sellerTokenAccount,
            mint: validMint,
            mintCheck: mintCheckPDA,
            treasuryData: treasuryPDA,
            treasury: treasury,
            devRoyaltyAccount: devRoyaltyAccount.publicKey,
            systemProgram: SystemProgram.programId,
            tokenProgram: TOKEN_PROGRAM_ID,
          },
          instructions: [
              anchor.web3.SystemProgram.createAccount({
                fromPubkey: seller.publicKey,
                newAccountPubkey: nftLocker.publicKey,
                lamports,
                space: 165,
                programId: TOKEN_PROGRAM_ID,
              }),
              TokenInstructions.initializeAccount({
                account: nftLocker.publicKey,
                mint: mint.publicKey,
                owner: treasuryPDA,
              }),
          ],
          signers: [seller, nftLocker],
        }
    );

    let _sellerDepositTokenAccount = await mint.getAccountInfo(
        nftLocker.publicKey
    );

    assert.ok(_sellerDepositTokenAccount.owner.equals(treasuryPDA));
    assert.ok(_sellerDepositTokenAccount.amount.toNumber() == 1);
  });

  it("withdraw nft", async () => {
    const lamports = await provider.connection.getMinimumBalanceForRentExemption(165);
    const withdrawToTokenAccount = anchor.web3.Keypair.generate();

    const [mintCheckPDA, _] = await PublicKey.findProgramAddress(
        [validMint.toBuffer()],
        program.programId
    );

    await program.rpc.withdrawNft({
          accounts: {
            authority: provider.wallet.publicKey,
            mint: validMint,
            mintCheck: mintCheckPDA,
            treasuryData: treasuryPDA,
            withdrawFromTokenAccount: nftLockerKey,
            withdrawToTokenAccount: withdrawToTokenAccount.publicKey,
            systemProgram: SystemProgram.programId,
            tokenProgram: TOKEN_PROGRAM_ID,
          },
          instructions: [
            anchor.web3.SystemProgram.createAccount({
              fromPubkey: provider.wallet.publicKey,
              newAccountPubkey: withdrawToTokenAccount.publicKey,
              lamports,
              space: 165,
              programId: TOKEN_PROGRAM_ID,
            }),
            TokenInstructions.initializeAccount({
              account: withdrawToTokenAccount.publicKey,
              mint: mint.publicKey,
              owner: provider.wallet.publicKey,
            }),
          ],
          signers: [withdrawToTokenAccount]
        }
    );

    let _withdrawToTokenAccount = await mint.getAccountInfo(
        withdrawToTokenAccount.publicKey
    );

    assert.ok(_withdrawToTokenAccount.owner.equals(provider.wallet.publicKey));
    assert.ok(_withdrawToTokenAccount.amount.toNumber() == 1);
  });

});